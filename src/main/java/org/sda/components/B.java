package org.sda.components;

import org.springframework.stereotype.Component;

@Component
public class B {

//    @Autowired
    private final C c;

    public B(C c) {
        this.c = c;
    }
}
