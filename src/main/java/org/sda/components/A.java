package org.sda.components;

import org.springframework.stereotype.Component;

@Component
public class A {

//    @Autowired
    private final B b;

    public A(B b) {
        this.b = b;
    }
}
