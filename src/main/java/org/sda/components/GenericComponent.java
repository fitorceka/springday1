package org.sda.components;

public class GenericComponent {

    public GenericComponent() {
        System.out.println("Generic Component ...");
    }

    public String getValue() {
        return "Hello from method";
    }
}
