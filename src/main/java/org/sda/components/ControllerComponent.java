package org.sda.components;

import org.springframework.stereotype.Controller;

@Controller
public class ControllerComponent {

    public ControllerComponent() {
        System.out.println("Controller Component ...");
    }
}
