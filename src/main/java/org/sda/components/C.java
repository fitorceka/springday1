package org.sda.components;

import org.springframework.stereotype.Component;

// used for demostartion of circular dependencies
@Component
public class C {

//    @Autowired
    private  A a;

//    public C(A a) {
//        this.a = a;
//    }
}
