package org.sda.value;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("app.properties")
//@PropertySources({ // used when out component uses many properties files
//
//})
public class Host {

//    @Value(value = "sdaal") //inject directly the values
    @Value("${sda.url}") // get the value from a properties file
    private String url;

    @Value("${user}")
    private String user;

    @Value("${password}")
    private String password;

//    public Host() {
//        System.out.println("URL: " + url);
//    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
