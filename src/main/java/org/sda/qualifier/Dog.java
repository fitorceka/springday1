package org.sda.qualifier;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary // used incase we do not have a Qualifier
public class Dog implements Animal {

    @Override
    public String sound() {
        return "Humhumhum";
    }
}
