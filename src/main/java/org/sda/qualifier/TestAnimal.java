package org.sda.qualifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestAnimal {

    private final Animal animal;

    @Autowired
    public TestAnimal(Animal animal) { //@Qualifier("wolf") used when we want to inject specific bean
        this.animal = animal;
        System.out.println(animal.sound());
    }
}
