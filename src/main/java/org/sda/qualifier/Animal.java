package org.sda.qualifier;

public interface Animal {

    String sound();
}
