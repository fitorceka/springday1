package org.sda;

import org.sda.components.GenericComponent;
import org.sda.lazy.LazyComponent;
import org.sda.scope.PrototypeClass;
import org.sda.scope.SingletonClass;
import org.sda.value.Host;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@SpringBootApplication
@ComponentScans(value = {
        @ComponentScan("org.sda"),
        @ComponentScan("out.sda")
})
public class SpringBootDayOne {

    public static void main(String[] args) {
        var context = SpringApplication.run(SpringBootDayOne.class);

        // get bean
//        GenericComponent gc = context.getBean(GenericComponent.class);
//        System.out.println(gc.getValue());

        //used for dependecy injection
//        ServiceComponent sc = context.getBean(ServiceComponent.class);
//        System.out.println(sc);

        GenericComponent gc = context.getBean(GenericComponent.class);

        // used to inject the field with a value
        Host h = context.getBean(Host.class);
        System.out.println("URL: " + h.getUrl());
        System.out.println("Username: " + h.getUser());
        System.out.println("Password: " + h.getPassword());


        // Scope
        SingletonClass s1 = context.getBean(SingletonClass.class);
        SingletonClass s2 = context.getBean(SingletonClass.class);
        SingletonClass s3 = context.getBean(SingletonClass.class);

        PrototypeClass p1 = context.getBean(PrototypeClass.class);
        PrototypeClass p2 = context.getBean(PrototypeClass.class);
        PrototypeClass p3 = context.getBean(PrototypeClass.class);

        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());
        System.out.println(s3.hashCode());
        System.out.println(p1.hashCode());
        System.out.println(p2.hashCode());
        System.out.println(p3.hashCode());


        // fetch lazily
        LazyComponent lz = context.getBean(LazyComponent.class);
    }
}
